#!/bin/bash
#: Title	: enter_existing_file.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Write a script that prompts the user to enter the name of a
#:+		: file. Repeat until the user enters a file that exists.
#: Options	: None

## Alternative approach to the solution
##read -p "Enter filename? " file
##until [ -e $file ]
##do
##	read -p "Enter filename? " file
##done

while :
do
	read -p "Enter filename? " file
	[ -e $file ] && break
done
