#!/bin/bash
#: Title	: passwd_length_check.sh
#: Date		: 2018-08-22
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Check that password length complies with minimum.
#: Options	: None

read password
if [ ${#password} -lt 8 ]
then
	printf "Password is too short: %d characters\n" "${#password}" >&2
	exit 1
fi
