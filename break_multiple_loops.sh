#!/bin/bash
#: Title	: break_multiple_loops.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Break out multiple nested loops using BREAK statement.
#: Options	: None

for n in a b c d e
do
	while true
	do
		if [ $RANDOM -gt 20000 ]
		then
			printf .
			break 2 ## break out of both while and for loops
		elif [ $RANDOM -lt 10000 ]
		then
			printf '"'
			break ## break out of the while loop
		fi
	done
done
