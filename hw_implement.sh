#!/bin/bash
#: Title	: hw_implement.sh
#: Date		: 2018-08-18
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Write a script to create the “Hello, World!” script, hw,
#:+		: in $HOME/bpl/bin/; make it executable; and then execute it.
#: Options	: None

TARGETDIR=$HOME/bpl/bin

echo 'printf "%s\n" "Hello, World!"' > $TARGETDIR/hw.sh
chmod +x $TARGETDIR/hw.sh
$TARGETDIR/hw.sh
