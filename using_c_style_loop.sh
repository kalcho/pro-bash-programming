#!/bin/bash
#: Title	: using_c_style_loop.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Using C style FOR looping.
#: Options	: None

for ((n=1; n<=10; ++n))
do
	echo "$n"
done
