#!/bin/bash
#: Title	: echo_randon_greater_then_20000.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Echo just random numbers greater then 20000.
#: Options	: None

for n in {1..9}
do
	x=$RANDOM
	[ $x -le 20000 ] && continue
	echo "n=$n x=$x"
done
